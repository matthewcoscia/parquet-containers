Configurations:
Apache Spark version: 2.3.4
    w/ Scala version: 2.11.8
Java JDK: 1.8.0_321
SBT version 1.6.2

Starting Files:
./lib/aut-0.17.1-DLRL.jar                               (to compile)
.lib/bin/ia-web-commons-1.1.0-jar-with-dependencies.jar (to run)
./src/main/scala/Warc2Parquet.scala
./build.sbt
./example_input.warc.gz

Building:
sbt package

Run with spark-submit:
spark-submit --class "Warc2Parquet" --master local[*] --jars lib/aut-0.17.1-DLRL.jar,lib\bin\ia-web-commons-1.1.0-jar-with-dependencies.jar target/scala-2.11/warc-2-parquet_2.11-1.0.jar ARCHIVEIT-1829-NONE-ITLSJV-20100331071451-00000-crawling09.us.archive.org-8090.warc.gz 

    
