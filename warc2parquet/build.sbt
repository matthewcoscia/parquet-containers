name := "WARC 2 Parquet"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.3.4"

scalacOptions += "-deprecation"
