import java.lang.System.nanoTime
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.io.{FileOutputStream, PrintStream, File}
import java.nio.file.{Paths, Files}
import java.util.regex.Pattern

import scala.math.pow
import scala.util.{Try,Success,Failure}
import scala.io.Source
import scala.io.StdIn._
import scala.util.control.Breaks._
import scala.collection.mutable.ListBuffer

import org.apache.spark.sql.types._
import org.apache.spark.sql.Row
import org.apache.spark.sql.DataFrameWriter
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.{SparkContext, SparkConf}

import org.apache.commons.io.FilenameUtils

import io.archivesunleashed._
import io.archivesunleashed.matchbox._

object Warc2Parquet
{
  /* master schema for AU conversion */
  val AUT_conversion_schema = StructType(
        List(
          StructField(name = "key", dataType = StringType, nullable = false),
          StructField(name = "surtUrl", dataType = StringType, nullable = false),
          StructField(name = "timestamp", dataType = StringType, nullable = false),
          StructField(name = "originalUrl", dataType = StringType, nullable = false),
          StructField(name = "mime", dataType = StringType, nullable = false),
          StructField(name = "status", dataType = StringType, nullable = false),
          StructField(name = "digest", dataType = StringType, nullable = false),
          StructField(name = "redirectUrl", dataType = StringType, nullable = false),
          StructField(name = "meta", dataType = StringType, nullable = false),
          StructField(name = "contentLength", dataType = LongType, nullable = false),
          StructField(name = "offset", dataType = LongType, nullable = false),
          StructField(name = "filename", dataType = StringType, nullable = false),
          StructField(name = "allheader", dataType = StringType, nullable = false),
          StructField(name = "payload", dataType = StringType, nullable = false)
      )
  )

  //Global variables
  val spark = SparkSession.builder.appName("Warc2Parquet").getOrCreate()
  // val spark = SparkSession.builder.appName("Warc2Parquet")
  //   .config("spark.executor.memory", "70g")
  //   .config("spark.driver.memory", "50g")
  //   .config("spark.memory.offHeap.enabled",true)
  //   .config("spark.memory.offHeap.size","16g")
  //   .getOrCreate()
  import spark.implicits._

  val sc = spark.sparkContext
  val sqlContext = spark.sqlContext

  // Search CWD for warc.gz files and convert each
  def main(args: Array[String])
  {
    val okFileExtensions = List("warc.gz")
    val files = getListOfFiles(new File(args(0)), okFileExtensions)
    println(s"$files")

    files.foreach( (f:File) => {
        val name = f.getPath()
        println(s"File: $name")
        run_conversion(name)
      }
    )
  }


  /************************************************
    ArchiveSpark (AS, WARC-CDX experiments) Version: 2.7.6
    ArchiveUnleashedToolkit (AUT, WARC experiments) Version: 0.17
  ************************************************/

  /************************************************
    Parquet/Avro File Generation and Configurations
  ************************************************/

  /* Generate Parquet file from WARC with customized AUT */
  def run_conversion(file: String)
  {
    val warcPath = file
    val name = FilenameUtils.getBaseName(warcPath)
    val outName = FilenameUtils.getBaseName(name)
    val outPath = "output/" + outName + ".parquet.gz"
    println(s"Input: $warcPath\n Output: $outName")

    // parquet writing conf
    sqlContext.setConf("parquet.dictionary.page.size", "5242880")
    sqlContext.setConf("parquet.block.size", "33554432")
    sqlContext.setConf("parquet.filter.statistics.enabled", "true")
    sqlContext.setConf("parquet.filter.dictionary.enabled", "true")
    sqlContext.setConf("spark.sql.parquet.outputTimestampType", "TIMESTAMP_MILLIS") // ensures correct timestamp format with predicate pushdown
    sqlContext.setConf("spark.sql.parquet.enableVectorizedReader", "false")

    // use AUT to convert WARC to Parquet/Avro
    val warc_rdd = { RecordLoader.loadArchives(warcPath, sc).map(r => Row((r.getSurt + r.getOffset.toString + outName),
                                                                           r.getSurt,
                                                                           r.getCrawlDate,
                                                                           r.getUrl,
                                                                           r.getMimeType,
                                                                           r.getHttpStatus,
                                                                           NulltoString(r.getDigest),
                                                                           r.getRedirect,
                                                                           r.getMeta,
                                                                           r.getLength,
                                                                           r.getOffset,
                                                                           outName,
                                                                           NulltoString(r.getHeaderFields),
                                                                           NulltoString(r.getContentString) ) )
                    }
    val warc_df = sqlContext.createDataFrame(warc_rdd,AUT_conversion_schema)
    // warc_df.coalesce(1).write.format("parquet").option("compression","gzip").mode("overwrite").save(outPath)

    val fixTimestampUDF = udf(fixTimestamp)
    // val parquet_df = spark.read.format("parquet").schema(AUT_conversion_schema).load(outPath)
    val fixedDf = warc_df.withColumn("timestamp",fixTimestampUDF(col("allheader"))).withColumn("timestamp",unix_timestamp($"timestamp", "yyyy-MM-dd HH:mm:ss").cast(TimestampType).as("timestamp"))

    try {
      fixedDf.coalesce(1).write.format("parquet").option("compression","gzip").mode("overwrite").save(outPath)
    } catch {
      case e: RuntimeException => println("Exception occurred writing to file $outPath :\n " + e)
    }
  }

  def NulltoString(n: Any): String = {
    if (n == null) {
      "-"
    }
    else {
      n.toString
    }
  }

  // note that AUT "r.getCrawlDate" function does not extract exact unix timestamp from the WARC,
  // for our experiments, we decide to extract the accurate timestamp from the WARC header as an additional step so that we can get the timestamp type column.
  // (this step can be optimized through modifying AUT to reduce multiple conversions)
  val fixTimestamp = (s:String) => {
      if (s != null){
          //println(s"\n$s.getClass.toString\n")
          val Pattern = "(((?:19|20)\\d\\d)-(0?[1-9]|1[012])-([12][0-9]|3[01]|0?[1-9]).*)".r
          Pattern.findFirstIn(s).getOrElse("null").replace("T"," ").replace("Z","")
      }
      else{
          //println(s"\nNo good\n")
          "null"
      }
  }

  def getListOfFiles(dir: File, extensions: List[String]): List[File] = {
      dir.listFiles.filter(_.isFile).toList.filter { file =>
          extensions.exists(file.getName.endsWith(_))
      }
  }
}
