from pyspark.sql import SparkSession
from pyspark.sql.types import *
spark = SparkSession.builder.master("local[*]").appName("Read Big JSON").getOrCreate()
sc = spark.sparkContext

schema = StructType([
    StructField("contributors",StringType(),True),
    StructField("created_at",StringType(),True),
    StructField("display_text_range", StructType([
         StructField("end", StringType(), True),
         StructField("start", StringType(), True)
    ])),
    StructField("entities", StructType([
         StructField("hashtags", ArrayType(StructType([
              StructField("indices", StringType(), True),
              StructField("text", StringType(), True)
         ])), True),
         StructField("media", ArrayType(StringType()), True),
         StructField("urls", ArrayType(StringType()), True),
         StructField("user_mentions", ArrayType(StringType()), True)
    ])),
    StructField("geo", StructType([
         StructField("country", StringType(), True),
         StructField("latitude", StringType(), True),
         StructField("longitude", StringType(), True),
         StructField("type", StringType(), True)
    ])),
    StructField("id", StringType(), True),
    StructField("in_reply_to_screen_name", StringType(), True),
    StructField("in_reply_to_status_id", StringType(), True),
    StructField("in_reply_to_user_id", StringType(), True),
    StructField("is_quote_status", StringType(), True),
    StructField("lang", StringType(), True),
    StructField("lang_iso_code", StringType(), True),
    StructField("metrics", StructType([
         StructField("favorite_count", LongType(), True),
         StructField("retweet_count", LongType(), True)
    ])),
    StructField("result_type", StringType(), True),
    StructField("source", StringType(), True),
    StructField("text", StringType(), True),
    StructField("user", StructType([
         StructField("contributors_enabled", StringType(), True),
         StructField("created_at", StringType(), True),
         StructField("default_profile", StringType(), True),
         StructField("default_profile_image", BooleanType(), True),
         StructField("description", StringType(), True),
         StructField("follow_request_sent", StringType(), True),
         StructField("geo_enabled", StringType(), True),
         StructField("has_extended_profile", StringType(), True),
         StructField("id", StringType(), True),
         StructField("is_translation_enabled", StringType(), True),
         StructField("is_translator", StringType(), True),
         StructField("lang", StringType(), True),
         StructField("location", StringType(), True),
         StructField("metrics", StructType([
              StructField("favorites_count", LongType(), True),
              StructField("followers_count", LongType(), True),
              StructField("friends_count", LongType(), True),
              StructField("listed_count", LongType(), True),
              StructField("statuses_count", LongType(), True)
         ])),
         StructField("notifications", StringType(), True),
         StructField("real_name", StringType(), True),
         StructField("screen_name", StringType(), True),
         StructField("time_zone", StringType(), True),
         StructField("translator_type", StringType(), True),
         StructField("url", StringType(), True),
         StructField("utc_offset", StringType(), True),
         StructField("verified", StringType(), True),
         StructField("withheld_in_countries",  ArrayType(StringType()), True)
    ]))
  ])

json_file = "json_examples/table_z_1.json"
df_with_schema = spark.read.option("multiline", "true").schema(schema).json( json_file )
df_with_schema.collect()
df_with_schema.printSchema()
df_with_schema.show()
