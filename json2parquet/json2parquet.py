from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import *
import os
import sys
import re
from os import path
import ntpath

# Global Variables
spark = SparkSession.builder.master("local[*]").appName("Read Big JSON").getOrCreate()
sc = spark.sparkContext

schema = StructType([
    StructField("contributors",StringType(),True),
    StructField("created_at",StringType(),True),
    StructField("display_text_range", StructType([
         StructField("end", StringType(), True),
         StructField("start", StringType(), True)
    ])),
    StructField("entities", StructType([
         StructField("hashtags", ArrayType(StructType([
              StructField("indices", StringType(), True),
              StructField("text", StringType(), True)
         ])), True),
         StructField("media", ArrayType(StringType()), True),
         StructField("urls", ArrayType(StringType()), True),
         StructField("user_mentions", ArrayType(StringType()), True)
    ])),
    StructField("geo", StructType([
         StructField("country", StringType(), True),
         StructField("latitude", StringType(), True),
         StructField("longitude", StringType(), True),
         StructField("type", StringType(), True)
    ])),
    StructField("id", StringType(), True),
    StructField("in_reply_to_screen_name", StringType(), True),
    StructField("in_reply_to_status_id", StringType(), True),
    StructField("in_reply_to_user_id", StringType(), True),
    StructField("is_quote_status", StringType(), True),
    StructField("lang", StringType(), True),
    StructField("lang_iso_code", StringType(), True),
    StructField("metrics", StructType([
         StructField("favorite_count", LongType(), True),
         StructField("retweet_count", LongType(), True)
    ])),
    StructField("result_type", StringType(), True),
    StructField("source", StringType(), True),
    StructField("text", StringType(), True),
    StructField("user", StructType([
         StructField("contributors_enabled", StringType(), True),
         StructField("created_at", StringType(), True),
         StructField("default_profile", StringType(), True),
         StructField("default_profile_image", BooleanType(), True),
         StructField("description", StringType(), True),
         StructField("follow_request_sent", StringType(), True),
         StructField("geo_enabled", StringType(), True),
         StructField("has_extended_profile", StringType(), True),
         StructField("id", StringType(), True),
         StructField("is_translation_enabled", StringType(), True),
         StructField("is_translator", StringType(), True),
         StructField("lang", StringType(), True),
         StructField("location", StringType(), True),
         StructField("metrics", StructType([
              StructField("favorites_count", LongType(), True),
              StructField("followers_count", LongType(), True),
              StructField("friends_count", LongType(), True),
              StructField("listed_count", LongType(), True),
              StructField("statuses_count", LongType(), True)
         ])),
         StructField("notifications", StringType(), True),
         StructField("real_name", StringType(), True),
         StructField("screen_name", StringType(), True),
         StructField("time_zone", StringType(), True),
         StructField("translator_type", StringType(), True),
         StructField("url", StringType(), True),
         StructField("utc_offset", StringType(), True),
         StructField("verified", StringType(), True),
         StructField("withheld_in_countries",  ArrayType(StringType()), True)
    ]))
  ])

def main():
    if len(sys.argv) != 2:
        print("Usage: python json2parquet.py [dir_name]")
        return

    dir_name = sys.argv[1]

    if not path.isdir(dir_name):
        print("Directory '%s' not found.\nUsage: python json2parquet.py [dir_name]" % (dir_name))
        return

    extractHashtagsUDF = udf(extractHashtags, ArrayType(StringType()))

    print("Beginning conversion of the json files in [%s]\nNote: We are assuming the json file fit the specified schema." % (dir_name))

    json_files = [file for file in os.listdir(dir_name) if file.endswith('.json')]
    for f in json_files:
        print("Converting [%s] to compressed parquet format" % (dir_name + "/" + f))
        df_json = spark.read.option("multiline", "true").schema(schema).json( dir_name + "/" + f )
        df_ht_json = df_json.withColumn("id_long",col("id").cast(LongType())) \
            .withColumn("timestampUTC_int64", unix_timestamp(col("user.created_at"), "yyyy-MM-dd HH:mm:ss"))  #\
            #.withColumn("entities_hashtags", extractHashtagsUDF(col("text")))
        name = ntpath.basename(f).split('.')
        output = "output/" + name[0] + ".parquet.gz"
        df_ht_json.write.option("compression", "snappy").mode("overwrite").parquet(output)
        print("Success! Output file:\t%s\n" % (output))

    print("Nice job! All of the json files have been converted.\nTo ensure expected results, "
          + "take a look at the parquet files in a dataframe.")

def extractHashtags(text):
    return re.findall(r"#(\w+)", text)

if __name__ == "__main__":
    main()
